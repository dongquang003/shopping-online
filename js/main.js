let getEle = function (ele) {
  return document.getElementById(ele);
};

// Danh sách sản phẩm
let listProd = new ListProduct();

// Danh sách sản phẩm có trong giỏ hàng
let listCart = new ListCart();

// Hiển thị danh sách sản phẩm lên màn hình
let fetchProd = function () {
  let resolver = function (res) {
    listProd.arr = res.data;
    renderHTML(listProd.arr);
    renderCart();
  };
  let rejecter = function (err) {
    console.log(err);
  };
  axios({
    url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/products",
    method: "GET",
  })
    .then(resolver)
    .catch(rejecter);
};
fetchProd();

// Render HTML
let renderHTML = function (list) {
  let contentHTML = "";
  for (let i = 0; i < list.length; i++) {
    contentHTML += `
            <div class="col-md-3" style="height:auto; border: 1px solid black; margin:auto 0; margin-bottom:10px;">
                <img src="${list[i].image}" style="height:280px; width:100%; margin-top:10px" />
                <p style="font-size:14px; text-align:center">${list[i].name}</p>
                <p style="font-size:14px">Mô tả: ${list[i].description}</p>

                <button class="btn btn-success" style="width:100%; margin-bottom:15px;" onclick="addToCart(${list[i].id})">Add to Cart</button>
            </div>
        `;
  }
  getEle("prod_content").innerHTML = contentHTML;
};

// Render Cart
let renderCart = () => {
  let contentHtml = "";
  let totalPrice = 0;
  for (let item of listCart.arr) {
    contentHtml += `
       <tr>
            <td>${item.name}</td>
            <td><img src="${item.image}" style="width: 100px;" alt="" /></td>
            <td>${item.price}</td>
            <td>
               <input type="button" onclick="updateQuantity('${
                 item.id
               }','minus')" id="plus" value="-" />
                <input type="text" id=${
                  "number" + item.id
                } style="width:50px" value="${item.quanlity}"/>
                <input type="button" onclick="updateQuantity('${
                  item.id
                }','plus')" value="+" />
            </td>
            <td>
            <button class="btn btn-danger" onclick="deleteProduct(${
              item.id
            })">Xoá</button>
            </td>
        </tr>
       `;
    console.log("quanlity", item.quanlity);

    totalPrice += parseInt(item.price) * item.quanlity;
  }
  getEle("cart_content").innerHTML = contentHtml;

  // In ra tổng tiền
  getEle("price").innerHTML = totalPrice;
};

// Xóa Sản Phẩm Từ Giỏ Hàng
let deleteProduct = function (id) {
  for (let i = 0; i < listCart.arr.length; i++) {
    if (+listCart.arr[i].id === id) {
      listCart.arr.splice(i, 1);
      setLocalStorage();
      renderCart();
    }
  }
};

/* ----------------------------- Thêm sản phẩm vào giỏ hàng ------------------------- */

let addNewProductToCart = function (id) {
  for (let prod of listProd.arr) {
    if (+prod.id === id) {
      let cartItem = new CartItem(
        prod.image,
        prod.id,
        prod.name,
        prod.price,
        1
      );
      listCart.arr.push(cartItem);
      setLocalStorage();
      renderCart();
      return;
    }
  }
};

let checkExistCart = function (id) {
  for (let cart of listCart.arr) {
    if (+cart.id === id) {
      cart.quanlity += 1;
      setLocalStorage();
      renderCart();
      return;
    }
  }
  addNewProductToCart(id);
};

let addToCart = function (prodId) {
  if (listCart.arr.length > 0) {
    checkExistCart(prodId);
  } else {
    addNewProductToCart(prodId);
  }

  console.log("Sản phẩm có trong giỏ hàng :", listCart.arr);
};

/* ---------------------------------------------^_^--------------------------------------------- */

// Cập nhật số lượng và render lại listCart
let updateQuantity = (id, type) => {
  var value = parseInt(document.getElementById("number" + id).value);
  //  Nếu value === true(không phải là số) thì value sẽ bằng 0 ngược lại nếu là số thì value =value
  value = isNaN(value) ? 0 : value;

  if (type == "plus") {
    value++;
    document.getElementById("number" + id).value = value;
  } else {
    if (value !== 1) {
      value--;
      document.getElementById("number" + id).value = value;
    }
  }

  let quanlityNum = +getEle("number" + id).value;
  for (let item of listCart.arr) {
    if (+item.id === +id) {
      item.quanlity = quanlityNum;
      setLocalStorage();
    }
  }
  renderCart();

  console.log("listCart:", listCart.arr);
};

// Thanh toán listCart, set listCart về rỗng
let payCart = () => {
  console.log(listCart.arr.length);
  if (listCart.arr.length === 0) {
    alert("vui lòng chọn sản phẩm muốn mua sau đó ấn thanh toán!!!");
  } else {
    listCart.arr = [];
    alert("Bạn đã thanh toán thành công!");
    localStorage.clear();
    renderCart();
  }
};

// Lưu giỏ hàng vào local storage
function setLocalStorage() {
  /**
   * Lưu mảng empl xuống localStorage
   * Khi lưu xuống ép sang kiểu string
   */
  localStorage.setItem("ListCart", JSON.stringify(listCart.arr));
}

// SetLocalStorage();
function getLocalStorage() {
  if (localStorage.getItem("ListCart")) {
    /**
     * Lấy mảng empl dưới localStorage lên dùng
     * Khi lấy lên để sử dụng ép sang kiểu Json
     */
    listCart.arr = JSON.parse(localStorage.getItem("ListCart"));
    renderCart();
  }
}
getLocalStorage();

// Tìm kiếm sản phẩm theo loại sản phẩm
let typeOfProd = function () {
  let newList = [];
  let type = getEle("typeOf").value;
  for (let item of listProd.arr) {
    if (item.type.toLowerCase() === type) {
      newList.push(item);
      renderHTML(newList);
    }
  }
  if (type === "show all") {
    renderHTML(listProd.arr);
  }
};

// Sắp xếp danh sách sản phẩm theo tên
let sortFunction = function (a, b) {
  let previous = a.name.toLowerCase();
  let next = b.name.toLowerCase();

  let comparison = 0;
  if (previous > next) {
    comparison = 1;
  } else if (previous < next) {
    comparison = -1;
  }
  return comparison;
};

let sortByName = function () {
  let result = listProd.arr.sort(sortFunction);
  renderHTML(result);
};

getEle("btnSort").addEventListener("click", sortByName);

getEle("typeOf").addEventListener("change", typeOfProd);
console.log(listCart.arr);
